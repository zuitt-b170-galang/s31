//routes - contains all the endpoints for the application; instead of putting a lot of routes in the index.js, we separate them in other file, routes.js (databaseRoutes.js i.e. tasksRoutes.js)

const express = require("express")

//creates a router instance as a middleware and routing system; also allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router()


const taskController = require("../controllers/taskControllers.js")


router.get("/",(req,res)=>{
	taskController.getAllTasks().then(result=>res.send(result))
})

router.post("/",(req,res)=>{
	taskController.createTask(req.body).then(result=>res.send(result))
})


router.delete("/:id", (req, res)=>{
	// URL parameter values are accessed through the request object's "params" property; the specified paramter/property of the object will match the URL parameter endpoint
	taskController.deleteTask(req.params.id).then(result => res.send(result))
})


router.put("/:id", (req, res)=>{
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})



router.get("/:id",(req,res)=>{
	taskController.getTask().then(result=>res.send(result))
})


router.put("/:id", (req, res)=>{
	taskController.updateTask(req.params.status, req.body).then(result => res.send(result));
})




module.exports = router