
const Task = require("../models/task.js")



module.exports.getAllTasks = () =>{
	return Task.find({}).then(result=>{
		return result;
	})
}

module.exports.createTask=(requestBody)=>{
	let newTask = new Task({
		name:requestBody.name
	})
	return newTask.save().then((savedTask,error)=>{
		if(error){
			console.log(error)
			return false
		}else{
			return savedTask
		}
	})
}

module.exports.deleteTask = (taskId) => {
	// findByIdAndRemove - finds the item to be deleted and removes it from the database; it uses id in looking for the document
	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
		if (error){
			console.log(error);
			return false
		}else{
			return removedTask
		}
	})
}

module.exports.updateTask = ( taskId, requestBody ) =>{
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error)
			return false
		} else {
			result.name = requestBody.name;
			return result.save().then((updateTask, error)=>{
				if (error) {
					console.log(error)
					return false
				}else{
					return updateTask
				}
			})
		}
	})
}


// ACTIVITY
/*
1 get a specific task in the database
    BUSINESS LOGIC
    "/:id"
    -find the id (findById())
    -return it as a response


2. update the status of a task
    BUSINESS LOGIC
    "/:id/complete"
    - find the id of the task
    - change the value of the status into "complete" using the requestBody
    - save the task    
*/


module.exports.getTask = (taskId)=>{
	return Task.findbyId(taskId).then((result,error)=>{
		if (error) {
					console.log(error)
					return false
		}else{
			return getTask
		}
	})
}

module.exports.updateTask = ( taskId, requestBody ) =>{
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error)
			return false
		} else {
			result.status = requestBody.status;
			return result.save().then((updateTask, error)=>{
				if (error) {
					console.log(error)
					return false
				}else{
					return updateTask
				}
			})
		}
	})
}